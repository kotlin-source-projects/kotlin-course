package com.cursokotlin.kotlincourse

fun main(){
    //Constantes Numericas
    val constantNumber1:Int = 1 
    val constantNumber2:Long = 1 
    val constantNumber3:Float = 1.0f 
    val constantNumber4:Double = 1.0
    //Constantes Alfanumericas
    val constantChar1:Char = 'a'
    val constantString1:String = "Hola mundo 2024"
    //Constantes Booleanas
    val constantBoolean1:Boolean = true
    val constantBoolean2:Boolean = false

    println("Constantes: los valores de las mismas no pueden ser modificados. Ejemplos:")
    println("- Numericas")
    println("\tInteger: $constantNumber1 \n\tLong: $constantNumber2 \n\tFloat: $constantNumber3 \n\tDouble: $constantNumber4\n")
    println("- Alfanumericas")
    println("\tChar: $constantChar1 \n\tString: $constantString1\n")
    println("- Booleanas")
    println("\tBoolean true: $constantBoolean1 \n\tBoolean false: $constantBoolean2\n")

    //Variables Numericas
    var variableNumber1:Int = 1 
    var variableNumber2:Long = 1 
    var variableNumber3:Float = 1.0f 
    var variableNumber4:Double = 1.0
    //Constantes Alfanumericas
    var variableChar1:Char = 'a'
    var variableString1:String = "Hola mundo 2024"
    //Constantes Booleanas
    var variableBoolean1:Boolean = true
    var variableBoolean2:Boolean = false

    println("Variables: los valores de las mismas si pueden ser modificados. Ejemplos:")
    println("- Numericas")
    println("\nValor inicial: ")
    println("\tInteger: $variableNumber1 \n\tLong: $variableNumber2 \n\tFloat: $variableNumber3 \n\tDouble: $variableNumber4\n")
    variableNumber1= 2
    variableNumber2 = 2
    variableNumber3 = 2.0f
    variableNumber4 = 2.0
    println("\nValor final: ")
    println("\tInteger: $variableNumber1 \n\tLong: $variableNumber2 \n\tFloat: $variableNumber3 \n\tDouble: $variableNumber4\n")
    println("- Alfanumericas")
    println("\nValor inicial: ")
    println("\tChar: $variableChar1 \n\tString: $variableString1\n")
    variableChar1 = 'b'
    variableString1 = "¡Hola mundo! 02/2024"
    println("\nValor final: ")
    println("\tChar: $variableChar1 \n\tString: $variableString1\n")
    println("- Booleanas")
    println("\nValor inicial: ")
    println("\tBoolean true: $variableBoolean1 \n\tBoolean false: $variableBoolean2\n")
    variableBoolean1 = false
    variableBoolean2 = true
    println("\nValor final: ")
    println("\tBoolean true: $variableBoolean1 \n\tBoolean false: $variableBoolean2\n")

}